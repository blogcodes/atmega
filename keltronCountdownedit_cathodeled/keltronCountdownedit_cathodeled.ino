/*
  keltronCountdown.ino

  Created: 2/3/2018 12:51:53 PM
  Author: JMT

*/


#include "main.h"
#include "time1.h"

/*int R[] = {2, 3, 4, 5, 6, 7, 8, 9, A3, A2, A1, A0, A4, A5, A6, A7};
  int colRed[] = {A15, A14, A13, A12, A11, A10, A9, A8, 17, 16, 15, 14, 13, 12, 11, 10};
  int colGreen[] = {37 , 36, 35, 34, 33, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22};*/

// 2-dimensional array of row pin numbers:
int R[] = {2, 3, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 30};
// 2-dimensional array of column pin numbers:
int col[] = {A7, A6, A5, A4, A3, A2, A1, A0, A15, A14, A13, A12, A11, A10, A9, A8};

/* Enable pins to enable buffer ICs cd74hc244  */
int EN11 = 26,  EN31 = 22 ;


#define RED_INTERRUPT 21
#define GREEN_INTERRUPT 19
#define YELLOW_INTERRUPT 18
int flag = 0;
volatile int flag_g = 0, flag_y = 0, flag_r = 0;
int a, b, c;
unsigned char current_interrupt;
int count_g = 0, count_r = 0, count_y = 0;
unsigned long last_red_timing_millisec = 0, last_green_timing_millisec = 0, last_yellow_timing_millisec = 0;
unsigned long current_red_timing_millisec = 0, current_green_timing_millisec = 0, current_yellow_timing_millisec = 0;
unsigned long timing_green, timing_red, timing_yellow, totalg, totaly, totalr;
volatile unsigned long timing_in_milliseconds, timing_g = 0, timing_y = 0, timing_r = 0, timing_green_new, timing_yellow_new, timing_red_new;

unsigned long allowed_threshold_diff = 1000;  // corresponds to 1 second

#define DEBUG

unsigned char numBuffer[10];





unsigned char arr[8][16][16] =
{
  //  //test bitmap,zeroth element
  {
    0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0,
    0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0,
    0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0,
    0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0,
    0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0,
    0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0,
    0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0,
    0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0,
    0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0,
    0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0,
    0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0,
    0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0,
    0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0,
    0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0,
    0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0,
    0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0
  },


  //unsigned char FULL[16][16] = //full bitmap
  {
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
  } ,




  // //bitmap for amber/yellow time
  {
    0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0,
    0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0,
    0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0,
    0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0,
    0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0,
    0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0,
    0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0,
    0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0,
    0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0

  },

  // //stop bitmap
  {

    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
    1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
    1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0,
    1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1,
    0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0,
    1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0,
    1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
  },

  //unsigned char WALK1[16][16] =   //walk1 bitmap
  {

    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0,
    0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0,
    0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1,
    0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0,
    0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0,
    1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0,
    1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0,
    0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
  },

  //unsigned char WALK2[16][16] =   //walk2 bitmap
  {

    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
    1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0,
    1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0,
    0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1,
    1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
  },

  //unsigned char STP[16][16] =       //STOP print
  {
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0,
    0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0,
    0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0,
    0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0,
    0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 1, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
  },
  //unsigned char GO[16][16] =       //GO bitmap
  {
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0,
    0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0,
    1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0,
    1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1,
    0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1,
    0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0,
    0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
    0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
    0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0,
    0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1,
    0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
  }
};

unsigned char digi[10][16][16] =
{
  //  //0 bitmap
  {
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
    0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
    0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0,
    0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
    0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
  },
  //   //1 bitmap
  {
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0,
    0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
    0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
  },

  //  //2 bitmap
  {
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0,
    0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0,
    0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0,
    0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0,
    0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
  },

  //     //3 bitmap
  {
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0,
    0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0,
    0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0,
    0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0,
    0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
  },

  // //4 bitmap
  {
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0,
    0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
    0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
    0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
  },

  //   //5 bitmap
  {
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0,
    0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0,
    0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0,
    0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0,
    0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
  },

  //     //6 bitmap
  {
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
    0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0,
    0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0,
    0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0,
    0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
  },

  //      //7 bitmap
  {
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0,
    1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0,
    1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0,
    0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
  },

  //    //8 bitmap
  {
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0,
    0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
    0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0,
    0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
    0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
  },

  //    //9 bitmap
  {
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0,
    0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0,
    0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0,
    0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
    0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
  }
};



void setup()
{
  Serial.begin(9600);

  pinMode(RED_INTERRUPT, INPUT_PULLUP);
  pinMode(GREEN_INTERRUPT, INPUT_PULLUP);
  pinMode(YELLOW_INTERRUPT, INPUT_PULLUP);

  // converting enable pins to outputs:

  pinMode(EN11, OUTPUT);
  pinMode(EN31, OUTPUT);

  // iterate over the pins:
  for (int i = 0; i < 16; i++)
    // initialize the output pins:
  {
    pinMode(R[i], OUTPUT);
    pinMode(col[i], OUTPUT);
  }
  TCCR1A = 0;
  TCCR1B = 0;

  TCNT1 = 0;            // preload timer 65536-16MHz/256/2Hz
  TCCR1B |= ((1 << CS10) | (1 << CS11)); // 64 prescaler
  TIMSK1 |= (1 << TOIE1);   // enable timer overflow interrupt


  attachInterrupt(digitalPinToInterrupt(RED_INTERRUPT), ISR_0, RISING);
  attachInterrupt(digitalPinToInterrupt(GREEN_INTERRUPT), ISR_1, RISING);
  attachInterrupt(digitalPinToInterrupt(YELLOW_INTERRUPT), ISR_2, RISING);


#ifdef DEBUG
  //Serial.println("I am working");
  delay(500);
#endif
  /*-----------------------------------------------------to flash all red LEDs-------------------------------------------------------*/

  for (int i = 0 ; i < 10 ; i++)       //Loop display i times
  {

    digitalWrite(EN11, HIGH);
    digitalWrite(EN31, LOW);
    Display(arr, 1);


  }
  Clear();
  /*----------------------------------------------------to flash all green LEDs----------------------------------------------------*/

  for (int i = 0 ; i < 10 ; i++)       //Loop display i times
  {

    digitalWrite(EN11, LOW);
    digitalWrite(EN31, HIGH);
    Display(arr, 1);

  }
  Clear();
}

void loop() {

  //timing_in_milliseconds++;
  timing_green = timing_green_new;
  timing_red  = timing_red_new;
  timing_yellow = timing_yellow_new;


  if ((flag_g == 1) &&  (timing_green < 100) && (flag_r == 0) && (flag_y == 0))
  {
    green();
    count_r = 0;
    count_y = 0;
    Serial.print("timing green=");
    Serial.println(timing_green);
    if (timing_green >= 10)
    {
      a = (timing_green / 10);
      b = (timing_green % 10);
      for (int n = 0 ; n < 50; n++)    //Loop display 50 times
      {

        if (((timing_green_new - timing_green) <= 3) && count_g == 0 )
        {
          Display(arr, 7);
        }
      }
      for (int i = a; (i > (a - 1)); i--)
      {
        for (int j = b - 1; (j >= 0); j--)
        {
          if (((timing_green_new - timing_green) <= 3) && count_g == 0)
          {
            for (int n = 0 ; n < 49 ; n++)    //Loop display 49 times
            {

              shiftleftDisplay(digi, i, j);
            }
          }
        }
      }



      for (int i = a - 1; (i >= 1); i--)
      {
        for (int j = 9; (j >= 6); j--)
        {
          if (((timing_green_new - timing_green) <= 3) && count_g == 0)
          {
            for (int n = 0 ; n < 49 ; n++)    //Loop display 49 times
            {

              shiftleftDisplay(digi, i, j);
            }
          }
        }
        if (((timing_green_new - timing_green) <= 3) && count_g == 0)
        {
          for (int n = 0 ; n < 50 ; n++)    //Loop display 50 times
          {

            Display(arr, 4);
          }
        }
        if (((timing_green_new - timing_green) <= 3) && count_g == 0)
        {
          for (int n = 0 ; n < 50 ; n++)    //Loop display 50 times
          {

            Display(arr, 5);
          }
        }


        for (int j = 3; j >= 0; j--)
        {
          if (((timing_green_new - timing_green) <= 3) && count_g == 0)
          {
            for (int n = 0 ; n < 49 ; n++)    //Loop display 49 times
            {
              shiftleftDisplay(digi, i, j);
            }
          }
        }
      }
      for (int p = 9; (p >= 0) ; p--)
      {
        if (((timing_green_new - timing_green) <= 3) && count_g == 0)
        {
          for (int n = 0; n < 50; n++)     //Loop display 50 times
          {

            Display(digi, p);
          }
        }
      }
      count_g = 1;
    }
    else if (timing_green < 10)
    {
      for (int j = timing_green; (j >= 0); j--)
      {
        if (((timing_green_new - timing_green) <= 3) && count_g == 0)
        {
          for (int n = 0; n < 50; n++)     //Loop display 50 times
          {
            Display(digi, j);
          }
        }

      }
      count_g = 1;
    }

    if ((abs(timing_green - totalg) >= 3) && (count_g == 1))
    {
      for (int i = 0 ; i < 10 ; i++)       //Loop display i times
      {


        Display(arr, 1);

      }
    }
    a = 0;
    b = 0;

  }


  else if ((flag_g == 1)  && (timing_green >= 100) && (flag_r == 0) && (flag_y == 0))
  {
    green();
    count_r = 0;
    count_y = 0;
    a = (timing_green / 100);
    b = ((timing_green % 100) / 10);
    c = ((timing_green % 100) % 10);
    if (((timing_green_new - timing_green) <= 3) && count_g == 0 )
    {
      for (int n = 0 ; n < 50 ; n++)       //Loop display 50 times
      {

        Display(arr, 7);
      }
    }

    for (int i = a; (i > a - 1); i--)
    {
      for (int j = b; (j > (b - 1)); j--)
      {
        for (int p = (c - 1); ( p >= 0); p--)
        {
          if (((timing_green_new - timing_green) <= 3) && count_g == 0 )
          {
            for (int n = 0; n < 49; n--)
            {
              shiftleftmostDisplay(digi, i, j, p);

            }
          }
        }
      }
      for (int j = (b - 1); (j >= 0); j--)
      {
        for (int p = 9; p >= 6; p--)
        {
          if (((timing_green_new - timing_green) <= 3) && count_g == 0 )
          {
            for (int n = 0; n < 49 ; n--)
            {
              shiftleftmostDisplay(digi, i, j, p);

            }
          }
        }
        if (((timing_green_new - timing_green) <= 3) && count_g == 0 )
        {
          for (int n = 0 ; n < 50 ; n--)
          {
            Display(arr, 4);
          }
        }
        if (((timing_green_new - timing_green) <= 3) && count_g == 0 )
        {
          for (int n = 0 ; n < 50 ; n--)
          {
            Display(arr, 5);
          }
        }

        for (int p = 3; (p >= 0); p--)
        {
          if (((timing_green_new - timing_green) <= 3) && count_g == 0 )
          {
            for (int n = 0; n < 49; n--)
            {
              shiftleftmostDisplay(digi, i, j, p);
            }
          }

        }
      }
    }

    for (int i = 9; (i >= 1); i--)
    {
      for (int j = 9; (j >= 6); j--)
      {
        if (((timing_green_new - timing_green) <= 3) && count_g == 0 )
        {
          for (int n = 0 ; n < 49 ; n++)       //Loop display 49 times
          {

            shiftleftDisplay(digi, i, j);
          }
        }
      }
      if (((timing_green_new - timing_green) <= 3) && count_g == 0 )
      {
        for (int n = 0 ; n < 50 ; n++)       //Loop display 50 times
        {

          Display(arr, 4);
        }
      }
      if (((timing_green_new - timing_green) <= 3) && count_g == 0 )
      {
        for (int n = 0 ; n < 50 ; n++)       //Loop display 50 times
        {

          Display(arr, 5);
        }
      }
      for (int j = 3; j >= 0; j--)
      {
        if (((timing_green_new - timing_green) <= 3) && count_g == 0 )
        {
          for (int n = 0 ; n < 49 ; n++)       //Loop display 49 times
          {
            shiftleftDisplay(digi, i, j);
          }
        }
      }
    }
    for (int j = 9; (j >= 0); j--)
    {
      if (((timing_green_new - timing_green) <= 3) && count_g == 0 )
      {
        for (int n = 0; n < 50; n++)        //Loop display 50 times
        {
          Display(digi, j);
        }
      }
    }
    count_g = 1;
    if ((abs(timing_green - totalg) >= 3) && (count_g == 1))
    {
      for (int i = 0 ; i < 10 ; i++)       //Loop display i times
      {


        Display(arr, 1);

      }
    }

    a = 0;
    b = 0;
    c = 0;
  }





  else if ((flag_r == 1)  && (timing_red < 100) && (flag_g == 0) && (flag_y == 0))
  {
    red();
    count_g = 0;
    count_y = 0;
    if (timing_red >= 10)
    {
      a = (timing_red / 10);
      b = (timing_red % 10);
      if (((timing_red_new - timing_red) <= 3) && (count_r == 0))
      {
        for (int n = 0 ; n < 50 ; n++)    //Loop display 50 times
        {

          Display(arr, 6);
        }
      }

      for (int i = a; (i > (a - 1)); i--)
      {
        for (int j = b - 1; (j >= 0); j--)
        {
          if (((timing_red_new - timing_red) <= 3) && (count_r == 0))
          {
            for (int n = 0 ; n < 49 ; n++)    //Loop display 49 times
            {

              shiftleftDisplay(digi, i, j);
            }
          }
        }
      }



      for (int i = a - 1; (i >= 1); i--)
      {
        for (int j = 9; (j >= 6); j--)
        {
          if (((timing_red_new - timing_red) <= 3) && (count_r == 0))
          {
            for (int n = 0 ; n < 49 ; n++)    //Loop display 49 times
            {

              shiftleftDisplay(digi, i, j);
            }
          }
        }
        if (((timing_red_new - timing_red) <= 3) && (count_r == 0))
        {
          for (int n = 0 ; n < 50 ; n++)    //Loop display 50 times
          {

            Display(arr, 3);
          }
        }
        if (((timing_red_new - timing_red) <= 3) && (count_r == 0))
        {
          for (int n = 0 ; n < 50 ; n++)    //Loop display 50 times
          {

            Display(arr, 6);
          }
        }
        for (int j = 3; (j >= 0); j--)
        {
          if (((timing_red_new - timing_red) <= 3) && (count_r == 0))
          {
            for (int n = 0 ; n < 49 ; n++)    //Loop display 49 times
            {
              shiftleftDisplay(digi, i, j);
            }
          }
        }
      }
      for (int j = 9; (j >= 0); j--)
      {
        if (((timing_red_new - timing_red) <= 3) && (count_r == 0))
        {
          for (int n = 0; n < 50; n++)     //Loop display 50 times
          {
            Display(digi, j);
          }
        }
      }
      count_r = 1;
    }
    else if (timing_red < 10)
    {
      for (int j = timing_red; (j >= 0); j--)
      {
        if (((timing_red_new - timing_red) <= 3) && (count_r == 0))
        {
          for (int n = 0; n < 50; n++)     //Loop display 50 times
          {
            Display(digi, j);
          }
        }
      }
      count_r = 1;
    }
    if ((abs(timing_red - totalr) >= 3) && (count_r == 1))
    {
      for (int i = 0 ; i < 10 ; i++)       //Loop display i times
      {


        Display(arr, 1);

      }
    }
    a = 0;
    b = 0;
  }


  else if ((flag_r == 1)  && (timing_red >= 100) && (flag_g == 0) && (flag_y == 0))
  {
    red();
    count_g = 0;
    count_y = 0;
    a = (timing_red / 100);
    b = ((timing_red % 100) / 10);
    c = ((timing_red % 100) % 10);
    if (((timing_red_new - timing_red) <= 3) && (count_r == 0))
    {
      for (int n = 0 ; n < 50 ; n++)       //Loop display 50 times
      {

        Display(arr, 6);
      }
    }

    for (int i = a; i > a - 1; i--)
    {
      for (int j = b; j > (b - 1); j--)
      {
        for (int p = (c - 1); p >= 0; p--)
        {
          if (((timing_red_new - timing_red) <= 3) && (count_r == 0))
          {
            for (int n = 0; n < 49; n--)
            {
              shiftleftmostDisplay(digi, i, j, p);

            }
          }
        }
      }
      for (int j = (b - 1); j >= 0; j--)
      {
        for (int p = 9; p >= 6; p--)
        {
          if (((timing_red_new - timing_red) <= 3) && (count_r == 0))
          {
            for (int n = 0; n < 49 ; n--)
            {
              shiftleftmostDisplay(digi, i, j, p);

            }
          }
        }
        if (((timing_red_new - timing_red) <= 3) && (count_r == 0))
        {
          for (int n = 0 ; n < 50 ; n--)
          {
            Display(arr, 4);
          }
        }
        if (((timing_red_new - timing_red) <= 3) && (count_r == 0))
        {
          for (int n = 0 ; n < 50 ; n--)
          {
            Display(arr, 5);
          }
        }

        for (int p = 3; p >= 0; p--)
        {
          if (((timing_red_new - timing_red) <= 3) && (count_r == 0))
          {
            for (int n = 0; n < 49; n--)
            {
              shiftleftmostDisplay(digi, i, j, p);
            }
          }

        }
      }
    }

    for (int i = 9; i >= 1; i--)
    {
      for (int j = 9; j >= 6; j--)
      {
        if (((timing_red_new - timing_red) <= 3) && (count_r == 0))
        {
          for (int n = 0 ; n < 49 ; n++)       //Loop display 49 times
          {

            shiftleftDisplay(digi, i, j);
          }
        }
      }
      if (((timing_red_new - timing_red) <= 3) && (count_r == 0))
      {
        for (int n = 0 ; n < 50 ; n++)       //Loop display 50 times
        {

          Display(arr, 4);
        }
      }
      if (((timing_red_new - timing_red) <= 3) && (count_r == 0))
      {
        for (int n = 0 ; n < 50 ; n++)       //Loop display 50 times
        {

          Display(arr, 5);
        }
      }
      for (int j = 3; j >= 0; j--)
      {
        if (((timing_red_new - timing_red) <= 3) && (count_r == 0))
        {
          for (int n = 0 ; n < 49 ; n++)       //Loop display 49 times
          {
            shiftleftDisplay(digi, i, j);
          }
        }
      }
    }
    for (int j = 9; j >= 0; j--)
    {
      if (((timing_red_new - timing_red) <= 3) && (count_r == 0))
      {
        for (int n = 0; n < 50; n++)        //Loop display 50 times
        {
          Display(digi, j);
        }
      }
    }
    count_r = 1;
    if ((abs(timing_red - totalr) >= 3) && (count_r == 1))
    {
      for (int i = 0 ; i < 10 ; i++)       //Loop display i times
      {


        Display(arr, 1);

      }
    }
    a = 0;
    b = 0;
    c = 0;
  }

  else if ((flag_y == 1) && (timing_yellow < 10) && (flag_g == 0) && (flag_r == 0))
  {
    red();
    count_r = 0;
    count_g = 0;
    for (int i = timing_yellow; i >= 0; i--)
    {
      if (((timing_yellow_new - timing_yellow) <= 3) && (count_y == 0))
      {
        for (int n = 0; n < 50; n++)        //Loop display 50 times
        {
          Display(arr, 2);
        }
      }
    }
    count_y = 1;
    if ((abs(timing_yellow - totaly) >= 3) && (count_y == 1))
    {
      for (int i = 0 ; i < 10 ; i++)       //Loop display i times
      {


        Display(arr, 2);

      }
    }
  }





}


void Display(unsigned char (*dat)[16][16], int k)
{

  for (int c = 0; c < 16; c++)
  {
    digitalWrite(R[c], LOW); //use c th column
    //loop

    for (int r = 0; r < 16; r++)
    {
      digitalWrite(col[r], dat[k][r][c]);
      //digitalWrite(col[r], *(*(*(arr+1)+r)+c));

    }
    delay(1);
    Clear();  //Remove empty display light
  }

}




/*void shiftleftDisplay(unsigned char dat2[16][16], unsigned char dat1[16][16])
  {
  for (int i = 0 ; i < 150 ; i++)       //Loop display i times
  {
    for (int c = 0; c < 16; c++)
    {
      digitalWrite(R[c], LOW); //use c-th row
      //loop
      for (int r = 0; r < 8; r++)
      {
        /* if ((r + 3) > 15)
          {

           digitalWrite(col[r], 0);
          }
          else
          {
        digitalWrite(col[r], dat2[r + 3][c]);
        //  }
      }
      for (int r = 8; r < 16; r++)
      {

        digitalWrite(col[r], dat1[r - 3][c]);
        //  }
      }

      // delay(1);
      Clear();  //Remove empty display light
    }
  }
  }*/


void shiftleftDisplay(unsigned char (*dat2)[16][16], int  k, int l)
{
  // for (int i = 0 ; i < 150 ; i++)       //Loop display i times
  //{
  for (int c = 0; c < 16; c++)
  {
    digitalWrite(R[c], LOW); //use c-th row
    //loop
    for (int r = 0; r < 8; r++)
    {
      /* if ((r + 3) > 15)
        {

         digitalWrite(col[r], 0);
        }
        else
        {*/
      digitalWrite(col[r], dat2[k][r + 3][c]);
      //  }
    }
    for (int r = 8; r < 16; r++)
    {

      digitalWrite(col[r], dat2[l][r - 3][c]);
      //  }
    }

    delay(1);
    Clear();  //Remove empty display light
  }
  // }
}


void shiftleftmostDisplay(unsigned char (*dat3)[16][16], int k, int l, int m)
{


  for (int c = 0; c < 16; c++)
  {
    digitalWrite(R[c], LOW); //use c-th row
    //loop
    for (int r = 0; r < 5; r++)
    {
      //if ((r + 6) > 15)
      //{

      // digitalWrite(col[r], 0);
      // }
      //else
      //{
      digitalWrite(col[r], dat3[k][r + 6][c]);
      // }
      for (int r = 4; r < 10; r++)
      {

        digitalWrite(col[r], dat3[l][r + 1][c]);

      }

      for (int r = 10; r < 16; r++)
      {

        digitalWrite(col[r], dat3[m][r - 5][c]);

      }

    }
    delay(1);
    Clear();  //Remove empty display light
  }
  // }

}




void Clear()
{
  for (int i = 0; i < 16; i++)
  {
    digitalWrite(col[i], LOW);
    digitalWrite(R[i], HIGH);
  }
}


void ISR_0() {
  flag_r = 1;
  flag_g = 0;
  flag_y = 0;
  timing_yellow_new = totaly;
  timing_y = 0;
  timing_green_new = totalg;
  timing_g = 0;
  /*#ifdef DEBUG
      //Serial.println("Got red interrupt");
    #endif


      //if (current_interrupt) {  // will not enter for first interrupt
      //if (current_interrupt == RED_INTERRUPT) {
      last_red_timing_millisec = current_red_timing_millisec;
      current_red_timing_millisec = timing_in_milliseconds;
    #ifdef DEBUG
      //	Serial.print("CurrentTiming_RED: ");
      //	Serial.print(current_red_timing_millisec);
      //
      //	Serial.print("   LastTiming_RED: ");
      //	Serial.print(last_red_timing_millisec);
      //
      //	Serial.print("   redDifference: ");
      //	Serial.println(fabs(current_red_timing_millisec - last_red_timing_millisec));
    #endif
      if (fabs(current_red_timing_millisec - last_red_timing_millisec) < allowed_threshold_diff) {
        //flag = 1;
        //	/	Serial.println(flag);
    #ifdef DEBUG
        ///		Serial.println("Got red consistently");
    #endif
      }
      else {
        Stop();
    #ifdef DEBUG
        ///		Serial.println("Red Inconsistently!!");
    #endif
      }*/


  current_interrupt = RED_INTERRUPT;
  timing_in_milliseconds = 0;
  //Serial.println(timing_in_milliseconds);
}


void ISR_1() {
  flag_g = 1;
  flag_r = 0;
  flag_y = 0;
  timing_red_new = totalr;
  timing_r = 0;
  timing_yellow_new = totaly;
  timing_y = 0;
  Serial.println("loop green");
  /*#ifdef DEBUG
      ///	Serial.println("Got green interrupt");
    #endif

     // last_green_timing_millisec = current_green_timing_millisec;
     // current_green_timing_millisec = timing_in_milliseconds;
    #ifdef DEBUG
      //	Serial.print("CurrentTiming_GREEN: ");
      //	Serial.print(current_green_timing_millisec);
      //
      //	Serial.print("   LastTiming_GREEN: ");
      //	Serial.print(last_green_timing_millisec);
      //
      //	Serial.print("   greenDifference: ");
      //	Serial.println(fabs(current_green_timing_millisec - last_green_timing_millisec));
    #endif
     // if (fabs(current_green_timing_millisec - last_green_timing_millisec) < allowed_threshold_diff) {
        //flag = 2;
        //		/Serial.println(flag);
    #ifdef DEBUG
        //		/Serial.println("Got green consistently");
    #endif
     /// }
     // else
      {
        Stop();
    #ifdef DEBUG
        //	/	Serial.println("Green Inconsistently!!");
    #endif
      }*/

  current_interrupt = GREEN_INTERRUPT;
  timing_in_milliseconds = 0;
  //Serial.println(timing_in_milliseconds);
}


void ISR_2() {
  flag_y = 1;
  flag_g = 0;
  flag_r = 0;
  timing_green_new = totalg;
  timing_g = 0;
  timing_red_new = totalr;
  timing_r = 0;
  /*#ifdef DEBUG
      //	Serial.println("Got yellow interrupt");
      //	Serial.println(flag);
    #endif*/

  current_interrupt = YELLOW_INTERRUPT;
  timing_in_milliseconds = 0;
  // Serial.println(timing_in_milliseconds);
}

ISR(TIMER1_OVF_vect)
{
  if ((flag_g == 1)  && (flag_r == 0) && (flag_y == 0))
  {
    timing_g++;
    totalg = (64.0 * 65536 * timing_g) / (16.0 * 1000000.0);
    Serial.println(timing_g);

  }
  if ((flag_y == 1)  && (flag_r == 0) && (flag_g == 0))
  {
    timing_y++;
    totaly = (64.0 * 65536 * timing_y) / (16.0 * 1000000.0);

  }

  if ((flag_r == 1) && (flag_g == 0) && (flag_y == 0))
  {
    timing_r++;
    totalr = (64.0 * 65536 * timing_r) / (16.0 * 1000000.0);

  }
}

void red()
{

  digitalWrite(EN11, HIGH);
  digitalWrite(EN31, LOW);


  //	Serial.print("LOOP-FLAG:  ");
  //	Serial.println(flag);
  /*    if (flag == 1)
      {

        shiftleftmostDisplay(ONE, EIGHT, ZERO);

      }

      else
        return;

      if (flag == 1)
      {
        //DisplayRed(FIVE);
        for (int i = 0 ; i < 5; i++)       //Loop display i times
        {
          Display(STP);
        }
      }

      if (flag == 1)
      {
        //DisplayRed(FIVE);
        for (int i = 0 ; i < 5 ; i++)       //Loop display i times
        {
          Display(STOP);
        }
      }

      if (flag == 1)
      {

        shiftleftDisplay(ONE, THREE);


      }
      else
        return;
      if (flag == 1)
      {

        shiftleftDisplay(ONE, TWO);


      }
      else
        return;
      if (flag == 1)
      {

        shiftleftDisplay(ONE, ONE);


      }
      else
        return;
      if (flag == 1)
      {

        shiftleftDisplay(ONE, ZERO);


      }
      else
        return;

      if (flag == 1)
        Display(NINE);
      else
        return;
      //	Serial.print("LOOP-FLAG:  ");
      //	Serial.println(flag);
      if (flag == 1)
        Display(EIGHT);
      else
        return;
      //	Serial.print("LOOP-FLAG:  ");
      //	Serial.println(flag);
      if (flag == 1)
      {
        //DisplayRed(SEVEN);
        Display(STP);
      }
      else
        return;
      //	Serial.print("LOOP-FLAG:  ");
      //	Serial.println(flag);
      if (flag == 1)
        Display(SIX);
      else
        return;
      //	Serial.print("LOOP-FLAG:  ");
      //	Serial.println(flag);
      if (flag == 1)
      {
        //DisplayRed(FIVE);
        Display(STOP);
      }
      else
        return;
      //	Serial.print("LOOP-FLAG:  ");
      //	Serial.println(flag);
      if (flag == 1)
        Display(FOUR);
      else
        return;
      //	Serial.print("LOOP-FLAG:  ");
      //	Serial.println(flag);
      if (flag == 1)
        Display(THREE);
      else
        return;
      //	Serial.print("LOOP-FLAG:  ");
      //	Serial.println(flag);
      if (flag == 1)
        Display(TWO);
      else
        return;
      //	Serial.print("LOOP-FLAG:  ");
      //	Serial.println(flag);
      if (flag == 1)
        Display(ONE);
      else
        return;
      //	Serial.print("LOOP-FLAG:  ");
      //	Serial.println(flag);
      if (flag == 1)
        Display(ZERO);
      else
        return;
      //	Serial.print("LOOP-FLAG:  ");
      //	Serial.println(flag);   */
}

void green()
{
  digitalWrite(EN11, LOW);
  digitalWrite(EN31, HIGH);


  //	Serial.print("LOOP-FLAG:  ");
  //	Serial.println(flag);
  /*    if (flag == 2)
      {


        shiftleftmostDisplay(ONE, EIGHT, ZERO);


      }
      else
        return;

      if (flag == 2)
      {
        //DisplayRed(FIVE);
        Display(GO);
      }

      if (flag == 2)
      {
        //DisplayRed(FIVE);
        Display(GO);
      }

      if (flag == 2)
      {

        shiftleftDisplay(ONE, THREE);


      }
      else
        return;
      if (flag == 2)
      {

        shiftleftDisplay(ONE, TWO);


      }
      else
        return;
      if (flag == 2)
      {

        shiftleftDisplay(ONE, ONE);


      }
      else
        return;
      if (flag == 2)
      {

        shiftleftDisplay(ONE, ZERO);


      }
      else
        return;
      if (flag == 2)
        Display(NINE);
      else
        return;
      //	Serial.print("LOOP-FLAG:  ");
      //	Serial.println(flag);
      if (flag == 2)
        Display(EIGHT);
      else
        return;
      //	Serial.print("LOOP-FLAG:  ");
      //	Serial.println(flag);
      if (flag == 2)
      {
        //DisplayGreen(SEVEN);
        walkDisplay(WALK1);
        walkDisplay(WALK2);
        walkDisplay(WALK1);
        walkDisplay(WALK2);
      }
      else
        return;
      //	Serial.print("LOOP-FLAG:  ");
      //	Serial.println(flag);
      if (flag == 2)
        Display(SIX);
      else
        return;
      //	Serial.print("LOOP-FLAG:  ");
      //	Serial.println(flag);
      if (flag == 2) {
        //DisplayGreen(FIVE);
        walkDisplay(WALK1);
        walkDisplay(WALK2);
        walkDisplay(WALK1);
        walkDisplay(WALK2);
      }
      else
        return;
      //	Serial.print("LOOP-FLAG:  ");
      //	Serial.println(flag);
      if (flag == 2)
        Display(FOUR);
      else
        return;
      //	Serial.print("LOOP-FLAG:  ");
      //	Serial.println(flag);
      if (flag == 2)
        Display(THREE);
      else
        return;
      //	Serial.print("LOOP-FLAG:  ");
      //	Serial.println(flag);
      if (flag == 2)
        Display(TWO);
      else
        return;
      //	Serial.print("LOOP-FLAG:  ");
      //	Serial.println(flag);
      if (flag == 2)
        Display(ONE);
      else
        return;
      //	Serial.print("LOOP-FLAG:  ");
      //	Serial.println(flag);
      if (flag == 2)
        Display(ZERO);
      else
        return;
      //	Serial.print("LOOP-FLAG:  ");
      //	Serial.println(flag);  */
}

void yellow()
{
  digitalWrite(EN11, HIGH);
  digitalWrite(EN31, LOW);

  /*   //	Serial.print("LOOP-FLAG:  ");
     //	Serial.println(flag);
     if (flag == 3) {
       Display(YELLOW);
     }
     else
       return;
     //	Serial.print("LOOP-FLAG:  ");
     //	Serial.println(flag);  */
}

void Stop()
{
  //flag_g = 0;
  //flag_r = 0;
  // flag_y = 0;
  /// Serial.println(flag);
}
